
설치와 동작

elasticsearch is installed at /usr/local/Cellar/elesticsearch and symbolic link /usr/local/bin/elasticsearch using brew inst$
# to start es: elasticsearch or $ES_PATH/bin/elasticsearch
# makesure set export ES_HOME= in addition to JAVA_HOME, PATH
# check with 'http://localhost:9200/?pretty'
# curl -i -XGET 'localhost:9200/_count?pretty' -d '{"query":{"match_all":{}}}'
# curl -XPOST 'http://localhost:9200/tutorial/helloworld/1' -d '{ "message": "Hello World!" }'

# es ubuntu install /usr/share/elasticsearch/  configuration is at /etc/init.d/elasticsearch, /etc/elasticsearch/elasticsearch$
# es msc configuration is at /usr/local/etc/elasticsearch



서비스로 start/stop하라고 되어져 있으나 &, ps aux|grep elasticsearch => (ps) kill -9 PID로 사용해도 됨....
(대량 연산중에는 문제가 있을수도 있음)  
sudo update-rc.d elasticsearch defaults 95 10
sudo /etc/init.d/elasticsearch start

backup & restore

1)register
curl -XPUT 'http://localhost:9200/_snapshot/asgard_backup' -d '{
    "type": "fs",
    "settings": {
        "compress" : true,
        "location": "/mnt2/elasticsearch/asgard/backup"
    }
}'


2)backup
$ curl -XPUT 'http://localhost:9200/_snapshot/asgard_backup/snapshot_1?wait_for_completion=false'

check result with curl -XGET 'http://localhost:9200/_snapshot/asgard_backup/_all'
or 하나만 본다면 ….GET _snapshot/my_backup/snapshot_2

부분 백업은 {  "indices": "index_1,index_2" }

3)restore
curl -XPOST 'http://localhost:9200/_snapshot/asgard_backup/snapshot_1/_restore'

부분 restore : 
다른 기계에서 복구: registering the repository containing the snapshot in the new cluster and starting the restore process

4)삭제
DELETE _snapshot/my_backup/snapshot_2
