/*
 	from chrome-extension://pgckigmaefoaemjpijdepakcghjkggmg/js/crawler.js,
 	chrome-extension://pgckigmaefoaemjpijdepakcghjkggmg/js/background.js
 	https://chrome.google.com/webstore/detail/social-analytics/pgckigmaefoaemjpijdepakcghjkggmg
  	
  linkedIn
  		url : 'https://www.linkedin.com/countserv/count/share?url=' + model.get('url') + '',
  		success : function(data) {
  					data.count
  pinterest
  		url : 'https://api.pinterest.com/v1/urls/count.json?url=' + model.get('url') + ''
  		success : function(data) {
  					data.count
  facebook
  		fb_url = "https://api.facebook.com/method/fql.query?" + "query=select%20total_count,like_count,comment_count,share_count" + "%20from%20link_stat%20where%20url='" + encodeURIComponent(model.get('url')) + "'&format=json";
  		if (fb_req.readyState == 4) { var resp = JSON.parse(fb_req.responseText); 
  			=>{facebookTotal: parseInt(resp[0].total_count)},{facebookLikes: parseInt(resp[0].like_count)}
  			=>{facebookComments: parseInt(resp[0].comment_count)}, {facebookShares: parseInt(resp[0].share_count)}
  		
  		
  		
  facebook2-Clicks
  		fb_url = "https://api.facebook.com/method/fql.query?" + "query=select%20click_count" + "%20from%20link_stat%20where%20url='" + encodeURIComponent(model.get('url')) + "'&format=json";
			=>{facebookClicks: parseInt(resp[0].click_count)}
  twitter
  		twitter_req.open("GET", "http://urls.api.twitter.com/1/urls/count.json?url=" + model.get('url'), true);
  			=>{twitterTweets: parseInt(resp.count)}
  
  
  현재 페이지 url 획득: 
  		chrome.tabs.get(tabId, function(tab) {...
  			selTab=tab => if(!self.ignoreUrl(selTab.url)) {  //넣어서 true가 아니면...
  			url=tab.url, urlEncoded: encodeURIComponent(selTab.url)
  
  		ignoreUrl : function(url) {
		if(url === "") return true;
		if(url.indexOf('chrome://') != -1) return true;
		if(url.indexOf('chrome-devtools') != -1) return true;
		if(url.indexOf('chrome-extension://') != -1) return true;
		if(url.indexOf('file://') != -1) return true;
		if(url.indexOf('http://localhost') != -1) return true;
		if((url.indexOf('google') != -1) && (url.indexOf('reader') != -1)) return true;
		if((url.indexOf('google') != -1) && (url.indexOf('search') != -1)) return true;
		return false;
	},
  
  값계산해서 model을 넣어서 model 값을 받아 온다 : SocialAnalytics.Crawler.Twitter(this=model);.......
  
  google analytics
  
  setup
  		_gaq.push(['_setAccount', 'UA-57453038-3']);
		_gaq.push(['_trackPageview']);
  		(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = 'https://ssl.google-analytics.com/ga.js';

				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
		})();
		
		
  
  		chrome.tabs.onSelectionChanged.addListener(function(tabId, selectInfo) {
  				_gaq.push(['_trackEvent', 'tab', 'selected']);
  		
  		chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  				if (changeInfo.status == 'complete' && tab.selected) {
  				
  						SocialAnalytics.Background.clearBadge();
						SocialAnalytics.Background.updateModel(tabId);
  						_gaq.push(['_trackEvent', 'tab', 'updated']);
 
 	
 SocialAnalytics.Background.updateGoogleResult('facebookLikes', data, tabId);  //var data = request.data; // from sender
 	updateGoogleResult : function(modelAttribute, data, tabId) {
 	googlemodel.on('change:' + modelAttribute, function() {
 		chrome.tabs.sendMessage(this.get('tabId'), {
				type: "google-result-response", 
				url: this.get('url'),
 								count: this.get(this.get('googleResult'))
 	
 	
 */


SocialAnalytics.Crawler = {
	LinkedIn : function(model) {
		model.set({linkedinShares: 0}, {silent: true});

		$.ajax({
			url : 'https://www.linkedin.com/countserv/count/share?url=' + model.get('url') + '',
			type: 'GET',
			async: false,
			contentType: "application/json",
			dataType : 'jsonp',
			success : function(data) {
				var share_count = 0;
				if(!isNaN(data.count)) {
					share_count = parseInt(data.count);
				}
				model.set({linkedinShares: share_count}, {silent: true});
				model.trigger('change:linkedinShares');
				model.calculateTotal();
			},
			error : function(jqXHR, textStatus, errorThrown) {
				model.set({linkedinShares: 0}, {silent: true});
				model.trigger('change:linkedinShares');
				model.calculateTotal();
			}
		});
	},

	Pinterest : function(model) {
		model.set({pinterestPins: 0}, {silent: true});

		$.ajax({
			url : 'https://api.pinterest.com/v1/urls/count.json?url=' + model.get('url') + '',
			type: 'GET',
			async: false,
			contentType: "application/json",
			dataType : 'jsonp',
			success : function(data) {
				var pin_count = 0;
				if(!isNaN(data.count)) {
					pin_count = parseInt(data.count);
				}
				model.set({pinterestPins: pin_count}, {silent: true});
				model.trigger('change:pinterestPins');
				model.calculateTotal();
			},
			error : function(jqXHR, textStatus, errorThrown) {
				model.set({pinterestPins: 0}, {silent: true});
				model.trigger('change:pinterestPins');
				model.calculateTotal();
			}
		});
	},

	
	
	Facebook : function(model) {
		model.set({facebookLikes: 0, facebookShares: 0, facebookComments: 0, facebookTotal: 0}, {silent: true});

		var fb_req = new XMLHttpRequest();
		var fb_url = "https://api.facebook.com/method/fql.query?" + "query=select%20total_count,like_count,comment_count,share_count" + "%20from%20link_stat%20where%20url='" + encodeURIComponent(model.get('url')) + "'&format=json";
		fb_req.open("GET", fb_url, true);
		fb_req.onreadystatechange = function() {
			if (fb_req.readyState == 4) {
				var resp = JSON.parse(fb_req.responseText);
				try {
					if (resp[0].total_count !== undefined) {
						model.set({facebookTotal: parseInt(resp[0].total_count)}, {silent: true});
						model.set({facebookLikes: parseInt(resp[0].like_count)}, {silent: true});
						model.set({facebookComments: parseInt(resp[0].comment_count)}, {silent: true});
						model.set({facebookShares: parseInt(resp[0].share_count)}, {silent: true});
						model.trigger('change:facebookTotal change:facebookLikes change:facebookComments change:facebookShares');
						model.calculateTotal();
					}
				} catch(e) {}

			}
		};
		fb_req.send();
	},

	FacebookClicks : function(model) {
		model.set({facebookClicks: 0}, {silent: true});

		var fb_req = new XMLHttpRequest();
		var fb_url = "https://api.facebook.com/method/fql.query?" + "query=select%20click_count" + "%20from%20link_stat%20where%20url='" + encodeURIComponent(model.get('url')) + "'&format=json";
		fb_req.open("GET", fb_url, true);

		fb_req.onreadystatechange = function() {
			if (fb_req.readyState == 4) {
				var resp = JSON.parse(fb_req.responseText);
				try {
					if (resp[0].click_count !== undefined) {
						model.set({facebookClicks: parseInt(resp[0].click_count)}, {silent: true});
						model.trigger('change:facebookClicks');
						model.calculateTotal();
					}
				} catch(e) {
				}
			}
		};
		fb_req.send();
	},

	Twitter : function(model) {
		model.set({twitterTweets: 0}, {silent: true});

		/*var twitter_req = new XMLHttpRequest();
		twitter_req.open("GET", "http://urls.api.twitter.com/1/urls/count.json?url=" + model.get('url'), true);
		twitter_req.onreadystatechange = function() {
			if (twitter_req.readyState == 4) {
				var resp = JSON.parse(twitter_req.responseText);
				try {
					if (resp.count !== undefined) {
						model.set({twitterTweets: parseInt(resp.count)}, {silent: true});
						model.trigger('change:twitterTweets');
						model.calculateTotal();
					}
				} catch(e) {}
			}
		};
		twitter_req.send();*/

		model.trigger('change:twitterTweets');
		model.calculateTotal();
	},

	
	Buffer: function(model) {

		$.getJSON("https://api.bufferapp.com/1/links/shares.json?url=" + model.get('url'))
			.success(function(data) {
				var bufferShares = data.shares;
				console.log("Buffer: " + bufferShares);
			}).error(function(data) {
				// console.log("Buffer error, data: " + data);
			});
	},

	Feedly: function(model) {

		var urlObj = this.urlObject({ 'url' : model.get('url') });
		var queryUrl = "http://" + urlObj.hostname;

		$.getJSON("http://feedly.com//v3/search/auto-complete?locale=null&query="+queryUrl+"&sites=3&topics=2&libraries=2&ct=feedly.mobile")
			.success(function(data) {
				if (data.sites.length > 0) {
					var subscribers = data.sites[0].subscribers;
					var website = data.sites[0].website;
					var twitterUser = data.sites[0].twitterScreenName;
					var facebookUser = data.sites[0].facebookUsername;
					var favicon = data.sites[0].iconUrl;
					console.log("Feedly: " + subscribers + " subscribers to " + website + " : " + favicon);
					console.log("Twitter: " + twitterUser);
					console.log("Facebook: " + facebookUser);
				}
			}).error(function(data) {
				// console.log("Buffer error, data: " + data);
			});
	},
	
	Weibo: function(model) {

		$.getJSON("https://api.weibo.com/2/short_url/shorten.json?url_long="+model.get('url')+"&access_token=1414437609673&source=8003029170")
			.success(function(data) {
				if (data.urls.length > 0) {
					var shortUrl = data.urls[0].url_short;

					$.getJSON("https://api.weibo.com/2/short_url/share/counts.json?url_short="+shortUrl+"&access_token=1414437609673&source=8003029170")
						.success(function(data) {
							if (data.urls.length > 0) {
								var weiboCount = data.urls[0].share_counts;
								console.log("Weibo: " + weiboCount);
							}
						}).error(function(data) {
						// console.log("Buffer error, data: " + data);
					});

				}
			}).error(function(data) {
			// console.log("Buffer error, data: " + data);
		});
	},
	
}
	
	
	
	
	
