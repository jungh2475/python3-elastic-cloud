source from https://blog.dcrucs.co/introduction-to-scrapy-in-python-76b2f373ff12#.y4j9yw69o
		https://github.com/dcrucsco/crawlbot

new source from : https://medium.com/@kaismh/extracting-data-from-websites-using-scrapy-e1e1e357651a#.2t8wcvg27
		using class SouqSpider(scrapy.Spider): => 명령어 scrapy runspider souq_spider.py -o deals.csv		
		

//setup
pip install scrapy
import scrapy from python3 console
scrapy startproject crawlbot  //프로젝트 이미 생성 되었음 , 이전에 이렇게 만든 것임 

cd crawlbot
scrapy genspider example example.com


crawlbot/spiders
	items.py
		from scrapy.item import Item, Field
		class CrawlbotItem(scrapy.Item):     # define the fields for your item here like:
    		url = scrapy.Field()
    		title = scrapy.Field()
    		description = scrapy.Field()
    	
    	rules = (Rule(LinkExtractor(allow=r'/article/'), callback='parse_item', follow=True),)
    	
    settings.py
    	USER_AGENT = 'crawlbot (+http://www.yourdomain.com)' 
    	# Configure maximum concurrent requests performed by Scrapy (default: 16)
		CONCURRENT_REQUESTS = 32
		DOWNLOAD_DELAY = 3
	
scrapy genspider -t crawl theonion theonion.com // Created spider 'theonion' using template 'crawl' in module
crawlbot.spiders.theonion
	def parse_item(self, response):
        i = CrawlbotItem()
        
        i['url'] = response.url
        
        i['title'] = response.xpath('//header[contains(@class, "content-header")]/h1/text()').extract()[0].strip()
        
        desc = response.xpath('//div[@class="content-text"]//p/text()').extract()
        
        desc = " ".join(desc)
        
        i['description'] = desc[0].strip()
        
        return i
        
	
	https://medium.com/@cog_g/scrapy-how-to-crawl-pages-from-a-listing-page-9c26f5fe3fc3#.fnbq53njh
	
	from scrapy.spider import BaseSpider
	from scrapy.selector import HtmlXPathSelector
 
	from deloitte_listing.items import DeloitteListingItem
 
	class DeloitteListingSpider(BaseSpider):
 
    	name = "deloitte_listing"
    	allowed_domains = ["deloitte.com"]
    	start_urls = [
        	"http://www.deloitte.com/view/fr_FR/fr/technology-fast-50/palmares/palmares-national/index.htm",
    	]
    	#alternative , reading from file 
    	f = open(“/home/scrapy/path/to/the/file/listing_total.txt”)
		start_urls = [url.strip() for url in f.readlines()]
		f.close()
 
    	def parse(self, response):
 			hxs = HtmlXPathSelector(response)
        	sites = hxs.select('//table[@class="custom_table"]/tr')
        	items = []
 
        for site in sites:
 
            #print site
            item = DeloitteListingItem()
            name = ''.join(site.select('./td/a/text()').extract())
            url = ''.join(site.select('./td/a/@href').extract())
            ca = ''.join(site.select('./td[4]/text()').extract())
            item['name'] = name
            item['url'] = url
            item['ca'] = ca
            items.append(item)
 
        return items


실행 명령어 
	scrapy crawl theonion -o items.json   //spider 이름  및 output 형식/파일명 


Scrapy shell can be used to test your XPath  //명령어 실험을 interactive하게 할수 있다 
scrapy shell http://www.deloitte.com/view/fr_FR/fr/technology-fast-50/palmares/palmares-national/index.htm
그리고 나서 XPath 명령어들을 실험해 보면 된다 >>> hxs.select(‘//title/text()’).extract()
Chrome may add html tags into the code display by the developer tools (F12). Be sure to look the “real” code (Ctrl+u) 
	
	