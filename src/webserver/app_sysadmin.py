'''
Created on Apr 26, 2017

@author: jungh

pip install Flask-Restless

'''

from flask import Flask
#import flask.ext.sqlalchemy
#import flask.ext.restless
from flask_sqlalchemy import SQLAlchemy
import flask_restless

app = Flask(__name__)

app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

##### class models #######
class SysRecord(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    
    
    def __init__(self, username, email):
        pass
    
    def __repr__(self):
        return '<User %r>' % self.id




manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)
#manager.create_api(Person, methods=['GET', 'POST', 'DELETE'])


if __name__ == '__main__':
    app.run()
    pass