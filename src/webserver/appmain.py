'''
Created on Oct 11, 2016

http://flask-sqlalchemy.pocoo.org/2.1/quickstart/


app.route('/report') -> method: get/post/put no delete 


@author: jungh
'''
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://username:password@localhost/db_name' # or pip install pymysql...> 'mysql+pymysql:,......app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)

    def __init__(self, username, email):
        self.username = username
        self.email = email

    def __repr__(self):
        return '<User %r>' % self.username


def initCreate(db):
    db.create_all()
    admin = User('admin', 'admin@example.com')
    guest = User('guest', 'guest@example.com')
    db.session.add(admin)
    db.session.add(guest)
    db.session.commit()
    pass


def initUpdate():
    pass


def querySample():
    users = User.query.all() #[<User u'admin'>, <User u'guest'>]
    admin = User.query.filter_by(username='admin').first()
    pass
