'''
Created on Oct 18, 2016

source from: https://daanlenaerts.com/blog/2015/06/03/create-a-simple-http-server-with-python-3/

@author: jungh
'''

#!/usr/bin/env python
 
from http.server import BaseHTTPRequestHandler, HTTPServer
#import urlparse 
# HTTPRequestHandler class
class MyHttpServer(BaseHTTPRequestHandler):
    

    # GET
    def do_GET(self):
        #parsed_path = urlparse.urlparse(self.path)
        if self.path=="/kjs":   ##if self.path.endswith("jpg"):
            self.path="/index_example2.html"
        try:
            
            self.send_response(200) # Send response status code        
            self.send_header('Content-type','text/html') # Send headers
            self.end_headers()
            # function here
            message = "Hello world!!!" # Send message back to client
            self.wfile.write(bytes(message, "utf8"))# Write content as utf-8 data
        
        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)
        return
 
    #def run(self):
        

 
if __name__ == "__main__": 
    print('starting server...')
    # server_address = ('127.0.0.1', 5245) #8081 or 8080 # Choose port 8080, for port 80, which is normally used for a http server, you need root access
    server_address = ('192.168.0.16', 5245) #내부 네트웍에서 나에게 할당된 ip를 적어야지 listening하게 된다. 
    httpd = HTTPServer(server_address, MyHttpServer)
    print('running server...')
    httpd.serve_forever()