'''
Created on Apr 13, 2017

@author: jungh
'''

class Scrapper(object):
    '''
    classdocs
    '''
    

    def __init__(self, loader,parser,outer):
        '''
        Constructor
        '''
        self.loader=loader
        self.parser=parser
        self.outer=outer
        self.config
        
    def load(self, config):  #load configuration file
        self.config=config
        pass
    
    def run(self):
        
        url=""  #from config
        respone=""        
        result=""
        
        #for-loop
        url=self.config.url
        response = self.loader(url)
        result=self.parser(response)
        self.outer.save(result)
        
        pass    
    
    
    
    
if __name__ == "__main__":
    pass