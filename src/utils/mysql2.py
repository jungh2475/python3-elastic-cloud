'''
Created on 25 Apr 2017

@author: junghlee
'''

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import pymysql

pymysql.install_as_MySQLdb()

app =  Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:1234@localhost/pyalchemy'
db = SQLAlchemy(app)

class Example(db.Model):
    __tablename__ ='example'
    id=db.Column('id', db.Integer, primary_key=True)
    data=db.Column('data',db.Unicode)
    
    def __init__(self,id_,data):
        self.id=id_
        self.data=data

if __name__ == "__main__":
    examples=Example.query.all()
    for ex in examples:
        print(ex.data)
    
    one=Example.query.filter_by(id=1).first()
    one.data
    
    #users = User.query.filter(User.active == True).all()
    # which is equivalent to...#users = db.session.query(User).filter(User.active == True).all()
    
    #insert, update
    new_ex=Example(5,'fifth')
    db.session.add(new_ex)
    db.session.commit()
    
    one.data ="newly updated"
    db.session.commit()
    
    #db.session.delete(obj)
    
    pass



'''
CREATE TABLE IF NOT EXISTS example( id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, data VARCHAR(20) NOT NULL );
INSERT INTO example(data) VALUES('hi');

http://fromleaf.tistory.com/197

try:

    import pymysql

    pymysql.install_as_MySQLdb()

except ImportError:

    from django.core.exceptions import ImproperlyConfigured

    raise ImproperlyConfigured("Error loading pymysql module: %s" % e)



출처: http://fromleaf.tistory.com/197 [중간어디쯤]


pip install PyMySQL
pip install --upgrade setuptools

MySQL-python 1.2.5 does not support Python 3
pip uninstall MySQL-python
brew install mysql
pip install MySQL-python
pip3.4 install --allow-external mysql-connector-python 

sudo -H pip install mysql-python

mysql installer
pip search mysql
pip search mysqldb
pip install MySQL-python
sudo apt-get install python-pip python-dev libmysqlclient-dev
brew install mysql-connector-c

'''