'''
Created on Dec 21, 2016

@author: jungh

http://jinja.pocoo.org/docs/dev/api/
'''
# -*- coding: utf-8 -*-

from jinja2 import Environment, PackageLoader

class HtmlRenderer(object):
    
    def __init__(self, apppyname, templatedir, htmlfile):
        self.env = Environment(loader=PackageLoader(apppyname, templatedir)) #scriptname.py,templatesPath 
    #looks up in the templates folder inside the yourapplication python package. 
        self.template=self.env.get_template(htmlfile) #template = Template('Hello {{ name }}!')
        pass
    
    def render(self, values):
        return self.template.render(values)  #return string
        #return print(self.template.render(msg="Hello World!")) or template.render({'knights': 'that say nih'})


if __name__ == "__main__": 
    
    pass 


