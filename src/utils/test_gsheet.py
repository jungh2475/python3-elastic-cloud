'''
Created on Oct 17, 2016

@author: jungh
'''



import gspread
from oauth2client.service_account import ServiceAccountCredentials

'''
    https://github.com/burnash/gspread 
    get Oauth2 credential :http://gspread.readthedocs.io/en/latest/oauth2.html with http://gspread.readthedocs.io/en/latest/oauth2.html
    myproject = jungh-1131-1131
    get json key service account from console.developers.google => You’ll need client_email and private_key
    pip install --upgrade oauth2client, pip install gspread, pip install PyOpenSSL 
    
    https://medium.com/@lchap/write-data-from-google-sheets-to-an-internal-database-f60dde03578b#.q7iki9qqe
    
    보안 로그인 방식은 sys_admin credential이므로 구글 콘솔에서 api enable후에 credential에서 sys_admin을 추가해 주어야 한다 
    make sure you added the sys_admin in your sheet to edit : 867067161628-compute@developer.gserviceaccount.com
    반드시 사용할 sheet가 등록된 gserviceaccount가 접근/편집 할수 있도록 추가가 되어져 있어야 한다 
    
'''
class Gsheet(object):



    def __init__(self, sheet_id='1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY'):
        self.scope=['https://spreadsheets.google.com/feeds']
        self.credentials = ServiceAccountCredentials.from_json_keyfile_name('My Project 1131-92e23d331383.json', self.scope)
        self.sheet_id=sheet_id
        self.subsheet_name=""
        gc = gspread.authorize(self.credentials)
        self.sheets=gc.open_by_key(sheet_id)
    
    def load_gsheets(self):
        return self.sheets
    
    def load_subsheet(self, subsheet_name):
        self.subsheet_name = subsheet_name
        return self.sheets.worksheet(subsheet_name)
        
        #scope =['https://spreadsheets.google.com/feeds'] 
        #credentials = ServiceAccountCredentials.from_json_keyfile_name('My Project 1131-92e23d331383.json', scope)
        
        #sheets=gc.open_by_key(sheet_urlid) # or gc.open_by_url('https://docs.google.com/spreadsheet/ccc?key=0Bm...FE&hl')
        #sheet=sheets.worksheet(subsheet_name) # or .get_worksheet(0), .sheet1, or get list of subsheets .worksheets()
    
        #val =sheet.acell('B1').value or .cell(1,2).value
        #sheet.update_acell('B2', "it's down there somewhere, let me take another look.")
        #cell_list = sheet.range('A1:B7')
        



if __name__ == '__main__':
    
    sheet_id='1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY'  #1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY, or kt.crawler.assignments.201610 #sheet_url='https://docs.google.com/spreadsheets/d/1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY/edit#gid=0'
    subsheet_name='assignments'
    
    #argument가 있는 경우 import sys, a=sys.argv[1]
    
    my_sheetsObj=Gsheet(sheet_id)
    my_sub_sheet=my_sheetsObj.load_subsheet(subsheet_name)
    temp_value=my_sub_sheet.cell(5,2).value
    print(temp_value)
    #sheets=load_gsheet(sheet_id)
    #my_sheet=sheets.worksheet(subsheet_name)
    #temp_val= my_sheet.cell(5,2).value
    #print(temp_val)
    
    