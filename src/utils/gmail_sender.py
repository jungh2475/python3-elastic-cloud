'''
Created on Dec 19, 2016

@author: jungh

https://www.google.com/settings/security/lesssecureapps
see more at http://naelshiab.com/tutorial-send-email-python/
https://docs.python.org/3/library/email-examples.html
'''


import smtplib


#python 2.7 from email.MIMEMultipart import MIMEMultipart, from email.MIMEText import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class gmail_sender(object):
    def __init__(self, uid, upassword):
        self.server= smtplib.SMTP('smtp.gmail.com', 587)
        self.uid=uid
        self.upassword=upassword
        self.server.starttls()
        self.server.login(self.uid, self.upassword)
 
    def build_msg(self):
        sender_mail="abc"
        msg="abc"
        subject="abc"
        pass
    
    def send_msg(self, sender_mail, body_text):
        self.server.sendmail(self.uid, sender_mail, body_text)  #, mail_options, rcpt_options)
    
    def server_close(self):
        self.server.quit()

class gmail_adv_sender(object):
    def __init__(self, uid, upassword):
        self.server= smtplib.SMTP('smtp.gmail.com', 587)
        self.uid=uid
        self.upassword=upassword
        self.server.starttls()
        self.server.login(self.uid, self.upassword)
        
    def build_msg(self, fromaddr, toaddr, subject, body):
        msg = MIMEMultipart()  #MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))
        return msg
    
    def send_msg(self, sender_mail, msg):
        self.server.sendmail(self.uid, sender_mail, msg.as_string())  #, mail_options, rcpt_options)
    
    def server_close(self):
        self.server.quit()



if __name__ == "__main__":
    pass
