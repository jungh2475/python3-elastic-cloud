'''
Created on Oct 17, 2016


1. check with curl 'http://localhost:9200/?pretty'
   if not start elasticsearch : cd elasticsearch-<version>, ./bin/elasticsearch
    두번쨰 노드 시작은 bin/elasticsearch -Des.node.name=Node-2 //http://localhost:9201.

https://elasticsearch-py.readthedocs.io/en/master/
setup.py or requirements.txt is:
# Elasticsearch 2.x
elasticsearch>=2.0.0,<3.0.0

-------------------------

$ curl -XGET 'http://localhost:9200/twitter/_search?q=user:kimchy'
$ curl -XGET 'http://localhost:9200/twitter/tweet,user/_search?q=user:kimchy'
$ curl -XGET 'http://localhost:9200/kimchy,elasticsearch/tweet/_search?q=tag:wow'
$ curl -XGET 'http://localhost:9200/_all/tweet/_search?q=tag:wow'
$ curl -XGET 'http://localhost:9200/_search?q=tag:wow'
$ curl -XGET 'http://localhost:9200/twitter/tweet/_search' -d '{
    "query" : {
        "term" : { "user" : "kimchy" }
    }
}
'

curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match_all": {} },
  "from": 10,
  "size": 10
}'

curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match": { "address": "mill" } }
}'

------------------------
backup and restore  PUT, GET
https://dzone.com/articles/introduction-elasticsearch-0
elasticsearch.yml => path.repo: ["/mount/backups", "/mount/longterm_backups"]
$ curl -XPUT 'http://localhost:9200/_snapshot/my_backup' -d '{
    "type": "fs","settings": { "location": "/mount/backups/my_backup", "compress": true }
}'
******"location"은 경로+파일명임. 원래 저장 경로에 저장하면 파일명만 적어도 됨 
$ curl -XGET 'http://localhost:9200/_snapshot/my_backup'
삭제도 가능: DELETE /_snapshot/my_backup

[Backup]

$ curl -XPUT 'http://localhost:9200/_snapshot/my_backup/snapshot_1' -d '{
  "indices": "index_1,index_2",
  "ignore_unavailable": true, "include_global_state": false
}'

[Check Get]
$ curl -XGET 'http://localhost:9200/_snapshot/my_backup'

[Restore]
$ curl -XPOST 'http://localhost:9200/_snapshot/my_backup/snapshot_1/_restore' -d '{
  "indices": "index_1,index_2",
  "ignore_unavailable": true, "include_global_state": false,
  "rename_pattern": "index_(.+)",
  "rename_replacement": "restored_index_$1"
}'

@author: jungh
'''

from datetime import datetime
from elasticsearch import Elasticsearch


#test if es is running
import requests
res = requests.get('http://localhost:9200')
print(res.content)



es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
# es = Elasticsearch([ {'host': 'localhost'},{'host': 'othernode', 'port': 443, 'url_prefix': 'es', 'use_ssl': True},])
# or 'http://user:secret@localhost:9200/

doc = {
    'author': 'kimchy',
    'text': 'Elasticsearch: cool. bonsai cool.',
    'timestamp': datetime.now(),
}
res = es.index(index="test-index", doc_type='tweet', id=1, body=doc)
res = es.get(index="test-index", doc_type='tweet', id=1)
print(res['_source'])
es.indices.refresh(index="test-index")
res = es.search(index="test-index", body={"query": {"match_all": {}}})
print("Got %d Hits:" % res['hits']['total'])
for hit in res['hits']['hits']:
    print("%(timestamp)s %(author)s: %(text)s" % hit["_source"])
    
# gmail을 로딩해서 검사해보는 예제 : https://bitquabit.com/post/having-fun-python-and-elasticsearch-part-1/
# 다른 사이트의 내용을 복사해서 주입 source from: https://tryolabs.com/blog/2015/02/17/python-elasticsearch-first-steps/
import json
r = requests.get('http://localhost:9200') 
i = 1
while r.status_code == 200:
    r = requests.get('http://swapi.co/api/people/'+ str(i))
    es.index(index='sw', doc_type='people', id=i, body=json.loads(r.content))
    i=i+1
 
print(i)


es.get(index='sw', doc_type='people', id=5)
es.search(index="sw", body={"query": {"match": {'name':'Darth Vader'}}})
es.search(index="sw", body={"query": {"prefix" : { "name" : "lu" }}})
