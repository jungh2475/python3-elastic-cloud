'''
Created on Dec 19, 2016

@author: jungh

http://www.voidynullness.net/blog/2013/07/25/gmail-email-with-python-via-imap/
https://docs.python.org/3/library/imaplib.html
https://gist.github.com/robulouski/7441883

imap 방식 대신에 구글에서 제공하는 google_api for python을 쓰는 것도 안정적일 것 같음(system_admin 만 가능) 
pip3 install -U google-api-python-client
https://developers.google.com/gmail/api/quickstart/python
https://developers.google.com/api-client-library/python/
https://developers.google.com/api-client-library/python/apis/gmail/v1
https://developers.google.com/gmail/api/guides/sending
https://developers.google.com/gmail/api/v1/reference/users/messages/get
    1)  Turn on the Gmail API -Download JSON) button to the right of the client ID.
    2)  def get_credentials():  return credentials  
            -> from apiclient import discovery, service = discovery.build('gmail', 'v1', http=http)
            -> results = service.users().labels().list(userId='me').execute()
'''

import sys
import imaplib
import getpass
import email
import datetime

M = imaplib.IMAP4_SSL('imap.gmail.com')
try:
    M.login('notatallawhistleblowerIswear@gmail.com', getpass.getpass())
except imaplib.IMAP4.error:
    print("LOGIN FAILED!!! ")


rv, mailboxes = M.list()
if rv == 'OK':
    print ("Mailboxes:")
    print (mailboxes)
    
rv, data = M.select("[Gmail]/Drafts")
num= data[0].split()[0]  # for num in data[0].split():
rv, data_msg = M.fetch(num, '(RFC822)')
data_msg[0][1] #data_msg[0][0]은 header 값이고, [1]이 실제 본문인듯함 
#from email.message import Message
msg = email.message_from_bytes(data_msg[0][1]) #msg = email.message_from_string(print(data[0][1])) # 
msg['Subject'] #msg['Date'], 
msg.keys() #['MIME-Version', 'Received', 'To', 'Date', 'Message-ID', 'Subject', 'From', 'Content-Type']
#이렇게 하면  본문이 없어진다. message_from_bytes에서 dictionary key값이 사라짐...