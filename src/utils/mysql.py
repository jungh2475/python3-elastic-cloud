'''
Created on Dec 21, 2016

@author: jungh

[설치 기초 ]
pip install mysqlclient
pip3 install mysqlclient
pip3 install mysql-connector

sudo apt-get install python3-pip
sudo apt-get install python3-dev libmysqlclient-dev

sudo apt-get install python-mysqldb  
##### xcode가 업데이트가 되면 재대로 설치가 안될수 있음  You should install MySQL through Homebrew first, to get python-mysql work properly on OS X. or for mac, pip uninstall MySQL-python, brew install mysqlpip install MySQL-python
xcode-select --install
pip install mysql-python

pip install sqlalchemy

print sqlalchemy.__version__

http://devyongsik.tistory.com/648
http://yujuwon.tistory.com/entry/SQLAlchemy-%EC%82%AC%EC%9A%A9%ED%95%98%EA%B8%B0
http://www.haruair.com/blog/1682

http://docs.sqlalchemy.org/en/latest/dialects/mysql.html
#####

'''
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
#Base.query = db_session.query_property()

class MySql(object):
    def __init__(self,db_config):
        host=db_config['host']
        dbname=db_config['dbname']
        uid=db_config['userid']
        upassword=db_config['password']
        s='mysql://'+uid+':'+upassword+'@'+host+'/'+dbname+'?charset=utf8' #'mysql://test:1234@localhost/test?charset=utf8'  # see http://docs.sqlalchemy.org/en/rel_0_9/core/engines.html
        #s='mysql://'+uid+':'+upassword+'@localhost/'+dbname+'?charset=utf8' #'mysql://test:1234@localhost/test?charset=utf8'  # see http://docs.sqlalchemy.org/en/rel_0_9/core/engines.html
        self.engine=create_engine(s,convert_unicode=False)
        self.db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=self.engine))
        self.Base = declarative_base()
    pass
    
    def load_db(self):
        #import sqlmodels
        self.Base.metadata.create_all(self.engine)
    
    def getData(self, query):
        return 123
        pass
    
    def setData(self, data):
        return 'y'

if __name__ == "__main__":
    pass