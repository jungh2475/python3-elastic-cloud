'''
Created on Dec 21, 2016

@author: jungh

from sqlalchemy import Column, Integer, String, DateTime
from mysql import Base

class TbTest(Base):
  _tablename_ = 'tbTable'
  id = Column(Integer, primary_key=True)
  datetime = Column(DateTime)
  string = Column(String(250))
  
  def __init__(self, datetime, string): #파라미터로 datetime과 string 값을 받는다
    self.datetime = datetime
    self.string = string
    
  def __repr__(self):
    return "<TbTest('%d', '%s', '%s'>" %(self.id, str(self.datetime), self.string)




class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)

    def __init__(self, name=None, email=None):
        self.name = name
        self.email = email

    def __repr__(self):
        return '<User %r>' % (self.name)



'''