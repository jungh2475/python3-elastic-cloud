'''
Created on Oct 10, 2016

https://doc.scrapy.org/en/latest/topics/practices.html

cron is a unix utility -tasks to be automatically run in the background at regular intervals
crontab -e  (file at /var/spool/cron/crontabs/[userName]) or put edit descriptions at /etc/cron.daily, .hourly, .monthly or /etc/cron.weekly
sudo crontab -l  //show all as list
/etc/cron.d or use crontab -e.

명령어 예시 >> */2 * * * * /usr/bin/python /home/souza/Documets/Listener/listener.py
    - 5 values: min hr day month (w_num or요일) 
    - 2가지를 체크해 볼것, 헤더 #!/usr/bin/python, 실행권한 chmod: chmod a+x foo.py

# pip install python-crontab
scrapy crawl myproject.com -o output.csv -t csv -s USER_AGENT="Mozilla...."

see more at https://potentpages.com/web-crawler-development/tutorials/scrapy/

@author: jungh
'''

#import scrapy
#from src.scrapy01.scrapy01.spiders.sample_spider import SampleSpider
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import logging
import smtplib

def getArguments():
    # sql에서 동작할 것들을 가져 오고 
    return 'hello'

def shouldIrun():
    return True

def crawler_run(argument):
    # from https://doc.scrapy.org/en/latest/topics/practices.html#run-scrapy-from-a-script
    # CrawlerProcess, Distributed crawls, Avoiding getting banned
    settings = get_project_settings()
    settings.set('USER_AGENT','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)')
    process = CrawlerProcess(settings)
    #process = CrawlerProcess({'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'})
    #process = CrawlerProcess(get_project_settings()) , from scrapy.utils.project import get_project_settings
    
    process.crawl("samplespider")
    #process.crawl(SampleSpider)  #sample_spider. MySpider  process.crawl('followallSpiderName', domain='scrapinghub.com')
    #process.crawl(MySpider2)
    process.start() # the script will block here until the crawling is finished

def sendmail(title, sender, receivers, message):
    #smtpObj = smtplib.SMTP( [host [, port [, local_hostname]]] ), smtplib.SMTP('mail.your-domain.com', 25)
    # from http://www.tutorialspoint.com/python/python_sending_email.htm
    message = """From: From Person <from@fromdomain.com>
                To: To Person <to@todomain.com>
                Subject: SMTP e-mail test
                
                This is a test e-mail message.
            """
    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, message)         
        print("Successfully sent email")
    except Exception:  #
        print ("Error: unable to send email")
    

if __name__ =='__main__':
    num =1
    #typical python logger
    logger = logging.getLogger()
    logger.info("starting main")
    if(shouldIrun()):
        argument = getArguments()
        crawler_run(argument)
    print ("iteration[",num,"] completed")    
    #now = time.localtime()
    #year = str(now.tm_year)

