'''
Created on Dec 22, 2016

@author: jungh
'''


import requests 
import json 
from bs4 import BeautifulSoup

class Scrap_cafe24(object):
    pass


'''
http://www.174.co.kr/product/detail.html?product_no=3073&cate_no=1&display_group=3
//*[@id="content"]/div/div[2]/div  or -> class="score-summary"  -> "score_summary_avg_score" -> "value"

(parent)//div[@id="content"]//div[@class="score-summary"]//
평점 평균 //div[@class="score_summary_avg_score"]//div[@class="value"]//text()
평점 갯수 //div[@class="score_summary_avg_score"]//div[@class="count"]//text()

//*[@id="content"]/div/div[3]/div[1]/div[1]/span[1]/span
리뷰숫자 //span[@class="reviews-count"]//text()

Q&A 숫자  //*[@id="qnaArea"]/div[1]/table/tbody/tr[1]/td[1]
//div[@id="prdQnA']//tr[@class="first xans-record-"]/tr/td/text()
마지막 기록 시간도 : ...tr/td[3]/text()
'''


if __name__ == "__main__":
    pass