'''
Created on Sep 30, 2016

    phantomJs browser를 사용해야 할수도 있음 
    
    photoid : /p/BKtvPaCA8yx/
    pageid: /helencranmer_floraldesign/
        yees1jl/followers/
    
    
    countdaily - likes,followers,comments by uid
    commentsPositivenessStrength -좋은글,열정적인 글들만 숫자를 센다 
    getkeywordsfromComments:
    search #hashtag and count all, topPopular, newly-added,  url: https://www.instagram.com/explore/tags/
    [3rd party] search shares of insta_post/photo from facebook,twitter 
    [3rd party] journal panel survey & monitoring
    engagementIndex_daily=(likes+comments)/followers
    
    https://pro.iconosquare.com/
    https://www.gundolle.com/auth/login-info
    
@author: jungh
'''

# source: https://medium.com/@pavlovdog/instagram-is-a-big-data-f8bc7a3e3dfa#.8te1rdcpc
# equivalent to : https://api.instagram.com/v1/users/USER_ID/requested-by?access_token=ACCESS_TOKEN
'''
    https://medium.com/@pavlovdog/instagram-is-a-big-data-f8bc7a3e3dfa#.8te1rdcpc
    using BeautifulSoup, 
    get access token of insta using : https://chrome.google.com/webstore/detail/instagram-for-chrome/opnbmdkdflhjiclaoiiifmheknpccalb?hl=en
    use this moduel : https://github.com/pavlovdog/Instagram-private-API/api.py 
        
        Get list of user's followers, Get list of user's followings //hub point인 사람인지 판단할수 있다 
'''

import requests 
import json 
from bs4 import BeautifulSoup

r = requests.get("https://www.instagram.com/leomessi/") 
html_code = r.text 
def extract_info_from_html(html): 
    soup = BeautifulSoup(html_code, 'html.parser') 
    script = soup.find_all('script')[6].string 
    script_data = json.loads(script[21:-1]) 
    user_data = script_data["entry_data"]["ProfilePage"][0]["user"]
    return user_data

print (extract_info_from_html(html_code))