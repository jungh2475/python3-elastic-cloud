'''
Created on Oct 11, 2016

from http://www.rmunn.com/sqlalchemy-tutorial/tutorial.html


    - pip install SQLAlchemy ..version check: print sqlalchemy.__version__
    - query = session.query(Member).filter(Member.created_at > datetime.datetime(2014, 1, 1)).limit(10) or .all()
    - @hybrid_property, def is_legit(self):
    

standard old-fashioned mySql connection : http://www.thegeekstuff.com/2016/06/mysql-connector-python/
import MySQLdb,db = MySQLdb.connect("localhost","testuser","test123","TESTDB" ),cursor = db.cursor(),try:cursor.execute(sql)
    
@author: jungh
'''

from sqlalchemy import create_engine, MetaData, Table, Column, String, Integer #from sqlalchemy import *
from sqlalchemy.orm import sessionmaker

def initUpdate():
    db = create_engine('sqlite:///tutorial.db')
    db.echo = True  # We want to see the SQL we're creating
    metadata = MetaData(db)
    users = Table('users', metadata, autoload=True)
    Session = sessionmaker(bind=db)
    session = Session() #session = create_session()
    
def initCreate():
    
    db = create_engine('sqlite:///tutorial.db') #mysql user: password "engine://user:password@host:port/database"
    #default: mysql://admin:1234@localhost:3036/kew1db, encoding='utf-8', echo=True  mysql-python:create_engine('mysql+mysqldb://s...
    metadata = MetaData(db)

    users = Table('users', metadata,
        Column('user_id', Integer, primary_key=True),
        Column('name', String(40)),
        Column('age', Integer),
        Column('password', String),
        mysql_engine='InnoDB',
        mysql_charset='utf8',
        mysql_key_block_size="1024"
    )
    users.create()


def writeDataDefault(users):
    i = users.insert()
    i.execute(name='Mary', age=30, password='secret')
    i.execute({'name': 'John', 'age': 42},
              {'name': 'Susan', 'age': 57},
              {'name': 'Carl', 'age': 33})

    s = users.select()
    rs = s.execute()
    
    '''
        session.add(user2)  # session.add_all([item1, item2, item3]) # session.delete(obj1)
        session.commit()  
    '''

def getQueryResult(users, query):  # see http://www.rmunn.com/sqlalchemy-tutorial/tutorial.html
    s = users.select(users.c.age < 40 & users.c.name != 'Mary')    #select
    rs = s.execute()  #rows = cursor returned
    for row in rs:  #row = rs.fetchone(), rows=rs.fetchall(), = row[3] = row['password']=row.password=row[users.c.password]
        print(row)
        print(row.name, 'is', row.age, 'years old')
    '''
        joined 
        emails =Table('emails', metadata,...Column('user_id', Integer, ForeignKey('users.user_id')),)
        s = select([users, emails], emails.c.user_id == users.c.user_id)
    '''

def getUrlsCrawl(machine_id):
    pass

def addProduct(products,product):
    i = products.insert()
    i.execute(name='Mary', age=30, password='secret')
    pass #facebookPageId, amazonProductId, regionSales, language

def addBrand(brand):
    pass


if __name__ == "__main__":
    #
    pass