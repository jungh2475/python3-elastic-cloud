'''
Created on Oct 17, 2016

init -configure the system (report destination-gSheet or url,  
load_gsheet (or CSV) : testcases, repetition & regression,  method(selenium, phantomjs) 
test_run
    inject, click, screenshot




@author: jungh
'''
import unittest
import requests
from requests.codes import * 
from scrapy import Selector

class MyTester(unittest.TestCase):
    
    #def __init__(self, testcases):
    #    self.testcases=testcases #[]
    #    pass
    
    def setUp(self):
        unittest.TestCase.setUp(self)
        pass
    
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        pass
    
    #def run_test(self):
    #    for testcase in self.testcases:
    #        print()
    #        print('hello,'+testcase)
    #    pass
    
    def test_1(self):
        first="a"
        second="a"
        msg="a testing"
        self.assertEqual(first, second, msg)
    
    #from here is subFunctions
    
    def load_url(self, url):
        reqObj=requests.Request()
        reqObj.cookies="aaaa"
        response=requests(reqObj)  #response = requests.get(url)
        s=response.status_code+response.text+response.headers['content-type']
        print(s)
    
    def getResponse_Xpath(self,url,xpath):
        response=requests.get(url)
        #r = requests.get('https://api.github.com/user', auth=('user', 'pass'))
        #r = requests.post('http://httpbin.org/post', data = {'key':'value'})
        #r = requests.get('http://httpbin.org/get', params=payload) #headers=headers
        # s = requests.Session()
        if (response.status_code!=200):  #requests.codes.ok 200
            return None
        sel=Selector(response)
        return sel.xpath(xpath).extract()    
        

if __name__ == "__main__": 
    unittest.main()
    pass

