'''
Created on 18 Oct 2016

source from: https://realpython.com/blog/python/getting-started-with-the-slack-api-using-python-and-flask/
pip install slackclient

https://api.slack.com/web
get token : 
kewtea(s101100), xoxp-22445334598-22438826660-92622931025-37778eaa5f2e9d7b14c0bd253c7327f1
sphouse(!Seri101100) xoxp-85923793553-85922988791-92618093285-fa9417a4a750e70255780b557e5ae302

kewtea #discussion
incoming webhook url: https://hooks.slack.com/services/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
curl -X POST --data-urlencode 'payload={"channel": "#inhouse", "username": "webhookbot", "text": "This is posted to #inhouse and comes from a bot named webhookbot.", "icon_emoji": ":ghost:"}' https://hooks.slack.com/services/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

./ngrok http 80
ngrok http -auth "user:password" 80

@author: junghlee
'''

import os
from slackclient import SlackClient

# (venv)$ export SLACK_TOKEN='your slack token pasted here'
SLACK_TOKEN = os.environ.get('SLACK_TOKEN')
kewtea_token='xoxp-22445334598-22438826660-92622931025-37778eaa5f2e9d7b14c0bd253c7327f1'
slack_client = SlackClient(kewtea_token) #SLACK_TOKEN 'your test token here'
slack_client.api_call("api.test")
slack_client.api_call("auth.test")



def list_channels():
    channels_call = slack_client.api_call("channels.list")
    if channels_call.get('ok'):
        return channels_call['channels']
    return None

def channel_info(channel_id):
    channel_info = slack_client.api_call("channels.info", channel=channel_id)
    if channel_info:
        return channel_info['channel']
    return None

def send_message(channel_id, message):
    slack_client.api_call(
        "chat.postMessage",
        channel=channel_id,
        text=message,
        username='pythonbot',
        icon_emoji=':robot_face:'
    )


if __name__ == '__main__':
    channels = list_channels()
    if channels:
        print("Channels: ")
        for channel in channels:
            print(channel['name'] + " (" + channel['id'] + ")")
            detailed_info = channel_info(channel['id'])
            if detailed_info:
                print('Latest text from ' + channel['name'] + ":")
                print(detailed_info['latest']['text'])
            if channel['name'] == 'general':
                send_message(channel['id'], "Hello " +
                             channel['name'] + "! It worked!")
        print('-----')
    else:
        print("Unable to authenticate.")

