'''
Created on 18 Oct 2016

source from : https://realpython.com/blog/python/getting-started-with-the-slack-api-using-python-and-flask/

slack post requires https, external ip address so use ngrok: localhost tunneling tool
[command , check status]  https://ngrok.com/ download and unzip
./ngrok help
./ngrok http 80                    # secure public URL for port 80 web server
./ngrok http 5000 
 register address https://6940e7da.ngrok.io at https://api.slack.com/outgoing-webhooks


ngrok이 싫으면  iptime ddns 사용 하세요 http://ccami.tistory.com/58
iptime firmware upgrade 필
http://192.168.0.1 => admin(1234)
고급>특수기능>DDNS 설정 iptime.org  ; kew1.iptime.org:3889 jungh.lee@kewtea.com(s101100)
고급>보안기능>공유기접속관리 : 원격관리 포트 사용 :555 (or 8080,3889 하나만 지정가능 )
고급>NAT라우터관리>포트포워드 설정 

python3 -m http.server 8080  python(2) -m SimpleHTTPServer 8080

218.153.100.86, ifconfig | grep "inet "

https://kewtea.slack.com/apps/manage
https://kewtea.slack.com/apps

outgoint payload receive
https://kewtea.slack.com/services/24025246133?updated=1#service_setup
token=Mz04DWXXfjr2x3Vn0AUltrDk
team_id=T0001
team_domain=example
channel_id=C2147483705
channel_name=test
user_id=U2147483697
user_name=Steve
command=/weather
text=94070
response_url=https://hooks.slack.com/commands/1234/5678


@author: junghlee
'''

import os
from flask import Flask, request, Response


app = Flask(__name__)

SLACK_WEBHOOK_SECRET = os.environ.get('SLACK_WEBHOOK_SECRET')


@app.route('/api/slack', methods=['POST'])
def inbound():
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        channel = request.form.get('channel_name')
        username = request.form.get('user_name')
        text = request.form.get('text')
        inbound_message = username + " in " + channel + " says: " + text
        print(inbound_message)
    return Response(), 200


@app.route('/', methods=['GET'])
def test():
    return Response('It works!')


if __name__ == "__main__":
    app.run(debug=True)
