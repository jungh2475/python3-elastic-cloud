'''
Created on Oct 5, 2016

    여러개의 계정으로 지정 사이트에서 like, share, click 등을 눌러준다 
    users, sites, urls(divs), coverage_ratio(slope)-확산속도, max_perday
    getUsersFromDB 
    


@author: jungh
'''
from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains

'''
    click(), click_and_hold(), release(), double_click, send_keys("")
'''

class ClickBot(object):
    #여기에는 class variable, check global
    
    
    def __init__(self,domain,assignment):
        self.domain=domain
        self.assignment=assignment
        self.status=""
        self.driver=webdriver.PhantomJS()
        self.driver.implicitly_wait(5)
        self.elements=[]
    
    def load_assignment(self,assignment):
        self.assignment=assignment
        self.run()
    

    def run(self):
        self.status="starting"
        self.status="completed"
        pass
    
    def load_elements(self, url, elements):  #[{'id':'abc'}, {'name':'bbb'}, {'xpath':'//text()'}] 
        #response=requests.get(url)
        self.driver.get(url)
        #return response
        for element in elements:
            #element=self.driver.find_element_by_id(element)
            
            self.elements.append('object')
    def run_actions(self, actions):
        for element in self.elements:
            element.send_keys(actions[0]['action'])
            
        self.submit.click()
        
if __name__ == "__main__": 
    myBot=ClickBot('facebook','abc')
    myBot.run()
    pass