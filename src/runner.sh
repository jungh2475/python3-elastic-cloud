#!/bin/bash

#crontab -e  # 5 * * * * /User/userName/webProjects/myProject/src/runner.sh
#crontab -e  # 5 * * * * ~/webProjects/myProject/src/runner.sh
#매시간 5분에 실행 
#하루에 한번 매일 4:30am=> 30 4 * * *
########
#5분마다 한번씩 : 0,5,10,15,20,25,30,35,40,45,50,55  * * * *  = */5 * * * *
TASKNAME='runner'
#SPATH='/User/userName/webProjects/myProject/src'  # script abs path without/end
SPATH='/Users/jungh/webProjects/python3-elastic-cloud/src'
LOGFILE=$TASKNAME-log.txt
TEMPFILE=$TASKNAME-temp.txt
ABSLOGFILE=$SPATH/$LOGFILE
echo 'checking logfile'
if [ ! -f $SPATH/$LOGFILE ] 
then
	echo "creating logfile"
	echo "$(date +'%Y-%m-%d %H:%M:%S'), $LOGFILE created" > $SPATH/$LOGFILE
fi
####################
function lgo {
	echo "$(date +'%Y-%m-%d %H:%M:%S'), $1" >> $SPATH/$LOGFILE
}

echo "crontab excuted"
echo "try to load temp from:"$SPATH/$TEMPFILE
if [ ! -f $SPATH/$TEMPFILE ] 
then 
	echo "# $(date +'%Y-%m-%d %H:%M:%S'),$TEMPFILE created" > $SPATH/$TEMPFILE
	echo "RUNCOUNT=0" >> $SPATH/$TEMPFILE
fi

source $SPATH/$TEMPFILE  # default RUNOUNT=0
################################

STR="run($RUNCOUNT)-$SPATH/runner.py"  #한줄로 붙여서 쓰자 
lgo $STR
echo $STR
#python3 $SPATH'runner.py' $RUNCOUNT $ARG2
STR='run('$RUNCOUNT')-runner.py-completed' # 한줄로 붙여서 쓰자 
lgo $STR
echo $STR
########## end of all runns ########
((RUNCOUNT=RUNCOUNT+1))
echo "RUNCOUNT $RUNCOUNT"
#temp file에 중간 진행 값들을 저장함
echo "# $(date +'%Y-%m-%d %H:%M:%S'),$TEMPFILE saved" > $SPATH/$TEMPFILE
echo "RUNCOUNT=$RUNCOUNT" >> $SPATH/$TEMPFILE
lgo 'all runs completed..goto sleep' # >> $SPATH/$LOGFILE
