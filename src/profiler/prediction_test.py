'''
Created on 18 Oct 2016

pip install numpy, scipy
pip install -U scikit-learn

pip install pip-review
pip-review --local --interactive
pip install -r requirements.txt --upgrade

from http://www.scipy-lectures.org/advanced/scikit-learn/

@author: junghlee
'''


from sklearn import datasets
from sklearn.tree import DecisionTreeClassifier

dataset = datasets.load_iris()
# iris.data.shape

model = DecisionTreeClassifier()
model.fit(dataset.data, dataset.target)

from sklearn import svm
clf = svm.LinearSVC()
clf.fit(dataset.data, dataset.target)
clf.predict([[ 5.0,  3.6,  1.3,  0.25]])

expected = dataset.target
predicted = model.predict(dataset.data)
#print(metrics.classification_report(expected, predicted))
#print(metrics.confusion_matrix(expected, predicted))