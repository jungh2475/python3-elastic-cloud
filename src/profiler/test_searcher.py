"""
    title
    description
    parameters
    returns
    exceptions

Created on Dec 8, 2016
@author: jungh

>>> IS=IndexSearcher(0)
>>> IS.search('b','z','k')
0

"""


#import sys , sys.path.append('/.../..'), from moduleName import className
#import importlib.util, import os, os.path.realpath or abspath

class IndexSearcher(object):
    """ 관심사항들이 어떤 도메인에서 몇등 위치인지 찾아 낸다     """
    def __init__(self,arg1):
        self.var1=0
    
    def search(self, domain, category, targets):
        """ 지정한 domain에 들어가서 category 안에서 targets들의 위치를 찾아 내어라
            google search에서 category_word를 넣었을때, targets(단어)의 위치
            amazon store에서 category에서
            :param arg1: the first value
            :return: return the value  
            :Example:
            >>> IS=IndexSearcher(2)
            >>> IS.search('k','k','k')
            0
        """
        #print("Hi Hi", domain,category,targets)
        self.var1=1
        return 0  #위치와, 발견시간?, 
    
    def test(self):
        a={'apple':2, 'samsung':3, 'kewtea':1}   # \줄을 넘길떄는 
        return a
    
    def target_history(self, domain, category, target, start_date, end_date):
        """ 일정 기간동안 target이 도메인 카테고리내에서의 랭킹 목록 출력 
            구현만 해놓았음, 반드시 검증할 필요 있음 
        """
        import pandas as pd
        import json
        self.var1=2
        # 1. 날짜로 먼져 발리고, 2. result안에 target이 있는 녀석만 남겨서 출력한다 
        df=pd.read_csv('file.csv') #.read_csv(f1, converters={'result':json.loads},header=0)        
        mask = (df['date'] > start_date) & (df['date'] <= end_date)
        df=df.loc[mask]
        #조건을 검사해서 true인것만 남기기, 옆처럼 간단히 쓰고 싶으나 안에 json으로 들어 있어서 꺼내 써야 하지 않는지?  : df=df[df.results[target] !=''] or df[df['result'][target] !=null] df.query('line_race != 0')
        df['result']=df['result'].apply(json.loads)
        # for loop를 쓰면 느리다 ...for index,row in df.iterrows():  #itertuples,  if target in row['result']:  #만약에 값이 존재하면 옮겨서 기록
        df=df[df['result'][target] !='0']  
        df['result']=df['result'][target]  #target값만 남긴다 
        #    print(index, row['domain'], row['category'])
               
        return df[['date','result']]   #.describe()해서 증감,편차를 구해 본다 
    
    def calculate_dynamics(self, dataframe):
        """ 주어진 기간동안 증감 meanSlope, 평균, 편차,    """
        dataframe.describe()
        # min() max()
        #dataframe['result'].mean()
        #dataframe['result'].std()
        #중간에 측정 안할 날이 있으므로, 기간 size? dataframe['result'][0]-[end]....count()
        return 0
    
    
    
if __name__ == "__main__":
    iS=IndexSearcher(0)
    print(iS.search('aa', 'bb', 'ccc'))
    
    
    import doctest
    #doctest.testfile("example.txt")
    doctest.testmod()
    #상세한 내용을 보고 싶을땐 이렇게 실행하세요. python3 -m doctest -v test_searcher.py
    
    
    """ 
        단어들을 만들어 내서, google search, facebook, amazon에서 각각의 랭킹을 조사하여, gSheet에 저장한다 
        
    """
    