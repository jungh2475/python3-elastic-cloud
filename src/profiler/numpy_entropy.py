'''
Created on 16 Oct 2016

@author: junghlee
'''




'''
pd.DataFrame(d, index=['d', 'b', 'a'])


import numpy as np

a=np.array([[1,2,3],[4,5,6],[7,8,9]])
c1=np.onces((3,3), dtype=int) 
d=np.arrange(0,5,1)  # start, end_before, stepSize
d=np.linspace(0,5,10) # start, stop_inclusive, numTotal
d.reshape(5,2)  # 5 row, 2 col
d.dim  ndim , shape, =>2 # dimension
d.size =>10
d.shape  =>, d.dtype, d.item
str_array=my_array.astype(str)
np.mean(dim2_array,axis=0) # y 축, column 별로 매기는 평균 
np.mean(dim2_array,axis=1) # x 축, row별로 매기는 평균 
np.sqrt(a), np.pi, np.sum(a), np.prod(a) 곱하기 
np.dot(a, b)

for(x,y) in my_array:

my_array[:2,:1] [row][column]

'''

'''
https://mariuszprzydatek.com/2014/10/31/measuring-entropy-data-disorder-and-information-gain/
entropy = -sum(p(x)*log2p(x))
information Gain(A,S) = entropy(S) -sum(p(t)*Entropy(t))
'''
import math
# Calculates the entropy of the given data set for the target attribute.
def entropy(data, target_attr):
 
    val_freq = {}
    data_entropy = 0.0
 
    # Calculate the frequency of each of the values in the target attr
    for record in data:
        if (val_freq.has_key(record[target_attr])):
            val_freq[record[target_attr]] += 1.0
        else:
            val_freq[record[target_attr]]  = 1.0
 
    # Calculate the entropy of the data for the target attribute
    for freq in val_freq.values():
        data_entropy += (-freq/len(data)) * math.log(freq/len(data), 2) 
 
    return data_entropy


# Calculates the information gain (reduction in entropy) that would result by splitting the data on the chosen attribute (attr).
def gain(data, attr, target_attr):
 
    val_freq = {}
    subset_entropy = 0.0
 
    # Calculate the frequency of each of the values in the target attribute
    for record in data:
        if (val_freq.has_key(record[attr])):
            val_freq[record[attr]] += 1.0
        else:
            val_freq[record[attr]]  = 1.0
 
    # Calculate the sum of the entropy for each subset of records weighted by their probability of occuring in the training set.
    for val in val_freq.keys():
        val_prob = val_freq[val] / sum(val_freq.values())
        data_subset = [record for record in data if record[attr] == val]
        subset_entropy += val_prob * entropy(data_subset, target_attr)
 
    # Subtract the entropy of the chosen attribute from the entropy of the whole data set with respect to the target attribute (and return it)
    return (entropy(data, target_attr) - subset_entropy)



'''
 ID3 decision tree
 https://mariuszprzydatek.com/2014/11/11/iterative-dichotomiser-3-id3-algorithm-decision-trees-machine-learning/
 
'''






