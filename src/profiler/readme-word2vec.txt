

사용예시

주제어에의한 article/product/sub_topic curation
	- subtopic words 추출 : 1)사람이 직접,...혹은 기계추천: hot_trend, vector_pattern(word2vec)
	- trend_topic_tab 단어 추출 : user_segment 선정 => 토픽단어 추출 (신규, hot & emerging) 
	- 

제품의 특성 추출 :
	- 소스: description(price, feature-keyword, product category/name/options), reviews,..... SNS, 연관 검색어 
	- 좋아하는/구매할 물건에 대해서 중요한 항목들 순서로 추출: ID3-entropy, 항목 correlation 측정*, 
	


사용자 특성 추출
	- 소스: browser history(page-keyword 추출), 쓴 글들
	- 기간별 관심사를 추출하여서, 내년 같은 기간의 행태를 예측 
	
	
playlist 만들기 
	

-------------------------------

구현 방식

0)데이터 표현 방식  vector: word2vec, doc2vec(word2vec), svd, svm  
1)기본 알고리즘: euclean_distnace, cosine_efficient-pearson, (x)correlation, ID3 classification 
ID3 -Gain Ratios entropy : p1....pn에서  px가 a/b/c 일때 각각 을 결정하는 가장 빠른 방법 
=> 얼마나 deterministic 한지 ...어떤 p가 더 중요한지 알려 줌. 
Latent Dirichlet allocation(LDA), TF-IDF , LSI(TFIDF+SVD)

spam filter : naive baysian,

1)모집단 구성 - taste group 구성 : 조사 실행 
2)preprocessing : 표준화(normalization)
2)


http://www.yittoo.com/blog/index.php/tag/word2vec/ 응용 예시들 
http://blog.buzzvil.com/2016/06/16/word2vec_content_clustering/
https://deeplearning4j.org/kr/word2vec

cosine similarity : sklearn.metrics.pairwise.cosine_similarity(X,Y) (n_samples_X, n_features)
http://blog.christianperone.com/2013/09/machine-learning-cosine-similarity-for-vector-space-models-part-iii/
from scipy import spatial
dataSetI = [3, 45, 7, 2]
dataSetII = [2, 54, 13, 15]
result = 1 - spatial.distance.cosine(dataSetI, dataSetII)
from sklearn.metrics.pairwise import cosine_similarity

In [24]: cosine_similarity([1, 0, -1], [-1,-1, 0])
Out[24]: array([[-0.5]])

word2vec -> word vector in 100 dimension  =>vec of wx
doc2vc  -> d=[w1,....wn] => sum of vec of w1..n

v1-v2 = x1-x2   : v1-v2+x2 = x1 

차원줄이기 SVD(특이값 분해), cosine 방향성 알아 내기 , PCA, NMF(non-neg matrix factori..비음수 인수분해, 데이터에서 주요 독립특성을 추, WxH=V(원본))
https://www.slideshare.net/madvirus/pca-svd, 차원축소 기법 훑어보기 PCA, SVD, NMF 최범균, 2014-04-25
sklearn.decomposition.PCA
pca = PCA(n_components=2)
pca.fit(X[, y])	Fit the model with X.
https://www.packtpub.com/mapt/book/big-data-and-business-intelligence/9781783989485/1/ch01lvl1sec21/using-truncated-svd-to-reduce-dimensionality

entropy
import pandas as pd
import scipy as sc

# Input a pandas series, one-dimensional labeled array check DataFrame(2-d), 
# pd.Series(np.random.randn(5), index=['a', 'b', 'c', 'd', 'e']), 여기서는 index가 필요가 없음 
def ent(data):
    p_data= data.value_counts()/len(data) # calculates the probabilities, descending order so that the first element is the most frequently-occurring element
    entropy=sc.stats.entropy(p_data)  # input probabilities to get the entropy 
    return entropy

pandas DataFrame :  df = pd.DataFrame(np.random.randn(6,4),index=dates,columns=list('ABCD'))  #날짜가 인덱스, 값은 A,B,C,D


------------------------

product = [a1, a2, ....an,...like/dislike]  attribute a
종(vertical 분석) a1의 값에서 mean/var찾아 보기 
 
user = [p1,p2.....,] p is property ( age, sex, local, language)
a.. or p...간에서 상관 관계 조사: correlation
user segment 내에서의 선호도를 여집합과의 차이와 비교하여 집단의 특성을 발견해 낸다




특정 그룹내에서의 관심사 추출 



http://www.markhneedham.com/blog/2016/07/27/scitkit-learn-tfidf-and-cosine-similarity-for-computer-science-papers/
https://vinta.ws/code/calculate-the-similarity-of-two-vectors.html
http://dataaspirant.com/2015/04/11/five-most-popular-similarity-measures-implementation-in-python/
from sklearn.metrics import  pairwise 
dist = pairwise.pairwise_distances(X[:,1:], main_object, metric='cosine')