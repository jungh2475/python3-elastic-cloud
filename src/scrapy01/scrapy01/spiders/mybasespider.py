'''
Created on Nov 24, 2016

run_by: scrapy crawl [Spidername], scrapy run ?
        scrapy shell 'http://quotes.toscrape.com/page/1/'  => response.css('title').extract()
        

@author: jungh
'''
import sys
#sys.path.append('../..')
sys.path.append("../")  #or export PYTHONPATH=/the/directory/where/your/modules/and/packages/are
from scrapy01.scrapy01.items import MyBaseItemLoader
from scrapy01.scrapy01.items import CategorySearchItem
import scrapy
from scrapy.loader import ItemLoader
#from ..scrapy01.items import CategorySearchItem
#from ...scrapy01.items import MyBaseItemLoader


class MyBaseSpider(scrapy.Spider):  
     # or CrawlSpider with rules=(Rule(LinkExtractor(allow=/deny=/callback=) no callback means follow=True
    name = "mybasespider"
    
    #allowed_domains = ["amazon.com"]
    
    def __init__(self):
        self.var1=1
    
    def load_urls(self, var_switch):
        urls = [
            'http://quotes.toscrape.com/page/1/',
            'http://quotes.toscrape.com/page/2/'
        ]
        return urls
    
    def start_requests(self):
        
        urls=self.load_urls(0)
        for url in urls:
            cookie_temp="ss"
            yield scrapy.Request(url, meta={'cookiejar': cookie_temp}, callback=self.parse_case_a)
            #read cookiejar : https://doc.scrapy.org/en/latest/topics/downloader-middleware.html?_ga=1.98699678.1807802268.1479954709#std:reqmeta-cookiejar
        '''
        return [scrapy.FormRequest("http://www.example.com/login",
                                   formdata={'user': 'john', 'pass': 'secret'},
                                   callback=self.logged_in)]
        
        
        '''
        #scrapy.Spider.start_requests(self)        
    
    def parse_case_a(self, response):
        #1. using itemloader 
        #loader = ItemLoader(item=CategorySearchItem(), response = response)  #ItemName
        loader = MyBaseItemLoader(item=CategorySearchItem(), response = response)
        loader.add_xpath('social', '//footer/a[@class = "social"]/@href')
        loader.add_css('name', '#productTitle ::text')
        loader.add_value('url', response.url)
        sub_loader = loader.nested_xpath('//footer')
        sub_loader.add_xpath('email', 'a[@class = "email"]/@href')
        return loader.load_item()
        
        #2. yield item or yield scrapy.Request(url,.., callback=self.parse)
        #    yield {'field':response.css().extract(),......} or
        #    item = SampleItem(), sel,  item['field']=sel.xpath().extract_first()
        # 
    def parse(self, response):
        
        pass
    
    