'''
Created on Oct 11, 2016

@author: jungh
'''

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule #scrapy.spiders.CrawlSpider
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time

'''

Created on Nov 23, 2016

@author: jungh


import time
from scrapy.spider import BaseSpider
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

class MySpider(BaseSpider):
    name = "my_spider"
    #allowed_domains
    
    def __init__(self):
        self.driver=webdriver.Firefox()  # or Chrominum
        self.driver.wait = WebDriverWait(self.driver,5) #time.sleep(5)

    def lookup(self, query):
        return 0
        
    def parse(self,response):
        self.driver.get(response.url)
        
    def closed(self, reason):
        self.driver.close()
        self.driver.quit()

'''

class FooSpider(CrawlSpider):
    name = 'foo'
    allow_domains = 'foo.com'
    start_urls = ['foo.com']
    
    
    rules = (
        # Extract links matching 'category.php' (but not matching 'subsection.php')
        # and follow links from them (since no callback means follow=True by default).
        Rule(LinkExtractor(allow=('category\.php', ), deny=('subsection\.php', ))),

        # Extract links matching 'item.php' and parse them with the spider's method parse_item
        Rule(LinkExtractor(allow=('item\.php', )), callback='parse_item'),
    )
    
    
    
    def parse_from_seleniumn(self, driver, parse):
        #http://danielfrg.com/blog/2015/09/28/crawling-python-selenium-docker/
        driver = webdriver.Remote(command_executor='http://192.168.99.101:4444/wd/hub',
        desired_capabilities=DesiredCapabilities.CHROME)
        #driver = webdriver.Firefox() 
        driver.get("http://www.yelp.com")
        best = driver.find_element_by_id('best-of-yelp-module')
        '''
        driver.find_element_by_xpath('//h1').text.
        do whatever you like to do 
        '''
        driver.close() 
    
    def parse_item(self, response):
        self.logger.info('Hi, this is an item page! %s', response.url)
        item = scrapy.Item()
        item['id'] = response.xpath('//td[@id="item_id"]/text()').re(r'ID: (\d+)')
        item['name'] = response.xpath('//td[@id="item_name"]/text()').extract()
        item['description'] = response.xpath('//td[@id="item_description"]/text()').extract()
        return item
    
    
    def __init__(self, *args, **kwargs):
        super(FooSpider, self).__init__(*args, **kwargs)
        self.download_delay = 0.25
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(60)

    def parse_foo(self, response):
        self.browser.get(response.url)  # load response to the browser
        button = self.browser.find_element_by_xpath("path") # find 
        # the element to click to
        button.click() # click
        time.sleep(1) # wait until the page is fully loaded
        source = self.browser.page_source # get source of the loaded page
        #sel = Selector(text=source) # create a Selector object
        #data = sel.xpath('path/to/the/data') # select data
        
