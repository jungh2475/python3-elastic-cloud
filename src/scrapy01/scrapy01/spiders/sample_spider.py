'''
Created on Oct 10, 2016


source : https://doc.scrapy.org/en/latest/intro/tutorial.html
source venv1/bin/activate
check directory or create if none: scrapy startproject tutorial
scrapy crawl quotes
scrapy crawl quotes -o quotes.json  = scrapy runspider quotes_spider.py -o quotes.json

scrapy shell 'http://quotes.toscrape.com/page/1/' =>response.css('title::text').extract(), .extract_first() or css('title::text')[0].extract()
 .re(r'Quotes.*'), response.xpath('//title/text()').extract_first()
 .css("div.tags a.tag::text").extract()=>answer where  <div class='tags'><a class='tag'>answer
 .response.css('li.next a::attr(href)').extract_first()=>'/page/2/' where <li class="next"><a href="/page/2/">Next <s
request, response, ,.....fetch(url), view(response)



scrapy commands
crawl spider
check -l //contract checks.


rules = [Rule(LinkExtractor(allow=['/technology-\d+']), 'parse_story')]   //allow argument contains a regular expression restricts our crawls to urls that match the expression ‘technology-‘ so we only scrape the technology articles


@author: jungh
'''
import scrapy
from scrapy.mail import MailSender

class SampleSpider(scrapy.Spider):
    name = "samplespider"
    handle_httpstatus_list = [301] #To parse any responses that are not 200 you need to add
    def start_requests(self):
        urls = [
            'http://quotes.toscrape.com/page/1/',
            'http://quotes.toscrape.com/page/2/',
        ]
        
        #Every spider instance has a logger within it and can used as follows:
        self.logger.info('Parse function called on %s', "response.url")
        
        for url in urls:
            yield scrapy.Request(url=url, meta={'dont_redirect':True}, callback=self.parse)  #yield scrapy.Request(url, meta={'dont_redirect':True})
        
        #    에러처리 errback=self.errback_httpbin, dont_filter=True)
        #    로거 self.logger.info('Recieved response from {}'.format(response.url))
        #    meta를 다르게 쓰는 법  
        #    request = scrapy.Request("http://www.something.com/some_page.html", callback=self.parse_page2)
        #    request.meta['item'] = item
        #    return request
        
        '''
        see all types of spiders at https://www.tutorialspoint.com/scrapy/scrapy_spiders.htm
        crawlSpider, csv, sitemap, 
        
        using CSVFeedSpider
        from scrapy.spiders import CSVFeedSpider
        from demoproject.items import DemoItem
        
            start_urls = ["http://www.demoexample.com/feed.csv"]
            delimiter = ";"
            quotechar = "'"
            headers = ["product_title", "product_link", "product_description"]
        def parse_row(self, response, row):
            item = DemoItem()
            item["product_title"] = row["product_title"]
            ......
            return item
        
        
        or using sitemapSpider
        '''
    def parse(self, response):
        
        if ".com" in response.url:
            from scrapy.shell import inspect_response
            inspect_response(response, self)
        
        
        for quote in response.css('div.quote'):
            yield {
                'text': quote.css('span.text::text').extract_first(),
                'author': quote.css('span small::text').extract_first(),
                'tags': quote.css('div.tags a.tag::text').extract(),
            }
        
        '''
        from scrapy.selector import Selector
        from scrapy.http import HtmlResponse
        
            response = HtmlResponse(url='http://mysite.com', body=body)
            Selector(response=response).xpath('//span/text()').extract()
        
        '''
        
        
        next_page = response.css('li.next a::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
        
        '''
        items = []
        for titles in titles:
            item = CraigslistSampleItem()
            ....
            items.append(item)
        return items;
        '''
        
        '''page = response.url.split("/")[-2]
        filename = 'quotes-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)
        '''
    
    def parse_author(self, response):
        def extract_with_css(query):  #내부 function 
            return response.css(query).extract_first().strip()

        yield {
            'name': extract_with_css('h3.author-title::text'),
            'birthdate': extract_with_css('.author-born-date::text'),
            'bio': extract_with_css('.author-description::text'),
        }
    
    
    def sendmail(self,sender,receivers,subject,message):
        mailer = MailSender() #MailSender.from_settings(settings)
        mailer.send(to=["someone@example.com"], subject="Some subject", body="Some body", cc=["another@example.com"])
        