# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join
#TakeFirst, MapCompose, Join=Returns the values joined with the separator given in the constructor, which defaults to u' '
#proc = MapCompose(filter_world, unicode.upper)

#class Scrapy01Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    #pass
from locale import currency


class ProductItem(scrapy.Item):
    # define the fields for your item here like:
    # product = Product(name='Desktop PC', price=1000), Product({'name': 'Laptop PC', 'lala': 1500}), product2 = Product(product) or .copy()
    # product['name'] or .get('','default'), product.keys(), product.items()
    
    name = scrapy.Field()
    sku= scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    stock = scrapy.Field()
    last_updated = scrapy.Field(serializer=str)
    timezone = scrapy.Field()
    pass

class CategorySearchItem(scrapy.Item):
    name = scrapy.Field()
    url = scrapy.Field()
    searchdomain = scrapy.Field()  #googlesearch, amazon, ......
    searchword = scrapy.Field()
    searchurl=scrapy.Field()  #or domain? 지역별로 다른 서버들이 있어서 
    results = scrapy.Field()
    timestamp = scrapy.Field()
    timezone = scrapy.Field()
    #pass

class MyBaseItemLoader(ItemLoader):
    
    default_output_processor = TakeFirst()
    
    #def get_collected_values(self, field_name)
    #    pass
    
    def add_fallback_css(self, field_name, css, *processors, **kw ):
        #if not any(self.get_collected_values(field_name)):
            self.add_css(field_name, css, *processors, **kw)
            




