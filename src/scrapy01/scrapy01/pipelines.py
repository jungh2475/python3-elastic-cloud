# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
import json

class Scrapy01Pipeline(object):
    def process_item(self, item, spider):
        #Price validation and dropping items with no prices
        if item['price']:
            if item['price_excludes_vat']:
                item['price'] = item['price'] * self.vat_factor
            return item
        else:
            self.write2json(item)
            raise DropItem("Missing price in %s" % item)
        
        #return item # or raise DropItem exception
    def write2json(self,item):
        line = json.dumps(dict(item)) + "\n"
        #self.db[self.collection_name].insert(dict(item))
        self.file.write(line)



class MyBase01Pipeline(object):  
    vat =2.25
    def __init__(self):
        self.file = open('items.jl', 'wb')
    # overriding functions : from_crawler, open_spider, close_spider....    
    def process_item(self, item, spider):
        if item['price']:
            if item['excludes_vat']:
                item['price'] = item['price'] * self.vat
            return item
        else:
            raise DropItem("Missing price in %s" % item)

#saving to elasticsearch


